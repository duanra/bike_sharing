#!usr/bin/env python3

import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 

from warnings import warn
from functools import wraps


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


rcMpl = {
	'text.latex.preamble'	: [r'\usepackage{newpxtext,newpxmath}'],
	'text.usetex'					: True,
	'font.family'					: 'serif'			
}


def show(pad=None, save=None, dpi=300):

	if pad is not None:
		plt.tight_layout(pad=pad)

	if save is not None:
		plt.savefig(save, dpi=dpi)

	plt.show()
	plt.close()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


def checkna(func):
	""" decorator for checking nan values in dataframe returned by func	"""

	@wraps(func)
	def wrapper(*args, **kwargs):
		
		df = func(*args, **kwargs)
		if df.isna().any().any():
			warn("nan values in {} returned by {}()".format(
				type(df), func.__name__
				))

		return df

	return wrapper


@checkna
def read_data(path, **kwargs):

	data = pd.read_csv(path, index_col=0, parse_dates=['dteday'], **kwargs)
	data['temp'] 	*= 41
	data['atemp'] *= 50
	data['hum'] 	*= 100
	data['windspeed'] *= 67

	return data


def get_season_period(data):

	ix = np.nonzero( np.diff(data['season']) )[0]
	ix = np.sort( np.concatenate((ix, ix+1)) ).tolist()
	ix = [0] + ix + [-1]

	return data['season'].iloc[ix]