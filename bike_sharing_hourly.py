""" explorative analysis and linear prediction model of the hourly count """
#!usr/bin/env python3

import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 
import seaborn as sns

from IPython import embed
from matplotlib.colors import LogNorm
from matplotlib.patches import Rectangle
from warnings import warn
from utils import *

from sklearn import linear_model, ensemble
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score, mean_absolute_error

pd.set_option('display.max_rows', 20)
plt.rcParams.update(rcMpl)


# # # # # # # # # # # # # # #  explorative analysis # # # # # # # # # # # # # # 

data = read_data('./data/hour.csv')


if False:

	plt.figure(figsize=(5,0.6*5))

	df = data[['hr', 'cnt', 'workingday']].copy()
	# shift from 0h-24h to 6h-24h-6h
	ix = (df.hr>=0) & (df.hr<=5)
	df.loc[ix, 'hr'] += 24

	sns.boxplot(
		x='hr', y='cnt', hue='workingday', data=df,
		hue_order=[1, 0], palette='muted', width=0.5, 
		linewidth=0.5, fliersize=1.5
	)

	plt.xticks(
		range(1, 25, 4),
		['07h', '11h', '15h', '19h', '23h', '01h', '03h']
	)

	ymin, ymax = plt.ylim()
	plt.gca().add_patch(Rectangle(
		xy=(12, ymin), width=12, height=ymax-ymin,
		facecolor='gray', alpha=0.3, zorder=0
	))

	lgd = plt.gca().get_legend_handles_labels()
	lgd_new = (lgd[0], ['yes', 'no'])
	plt.legend(*lgd_new, title='working day', loc='upper right')

	plt.xlabel('daily hour')
	plt.ylabel('count of rented bikes/hour')

	show(pad=0.1, save='./figs/cnt_hourbin.pdf')



if False:

	plt.figure(figsize=(5,0.6*5))

	df = data.groupby(['weathersit', 'cnt']).size().reset_index(name='density')
	df['xlocs'] = df['weathersit'] + np.random.uniform(-0.25, 0.25, len(df))

	# mimic sns.stripplot() while handling of relative densities
	sns.scatterplot(
		x='xlocs', y='cnt', size='density', hue='weathersit',
		data=df, sizes=(5, 120), palette='muted',
		linewidth=0.5, alpha=0.9, legend=False
	)

	gb = data.groupby('weathersit')
	nfrac_weathersit  = gb.size() / len(data)
	bbox = dict(color='grey', lw=0, alpha=0.5, boxstyle='round')
	plt.text(1-0.15, 1000, '{:.1f}\%'.format(100*nfrac_weathersit[1]), bbox=bbox)
	plt.text(2-0.15, 950, r'{:.1f}\%'.format(100*nfrac_weathersit[2]), bbox=bbox)
	plt.text(3-0.15, 850, r'{:.2f}\%'.format(100*nfrac_weathersit[3]), bbox=bbox)
	plt.text(4-0.15, 450, r'{:.2f}\%'.format(100*nfrac_weathersit[4]), bbox=bbox)

	cnt_average = gb.mean().cnt
	plt.hlines(
		cnt_average, 0.5, cnt_average.index,
		colors=sns.color_palette('muted'),
		linestyles='dashed', lw=0.8
	)

	plt.xlabel('weather condition')
	plt.xticks(range(1,5), range(1,5))
	plt.xlim(0.5, 4.5)
	plt.ylabel('count of rented bikes/hour')
	plt.ylim(top=1100)

	show(pad=0.1, save='./figs/cnt_weathersitbin.pdf')



if False:

	# plt.figure(figsize=(4,0.9*4))
	# sns.scatterplot(
	# 	x='temp', y='cnt', hue='yr', size='weathersit', data=data, 
	# 	palette=['purple', 'seagreen'], s=2, alpha=0.7,
	# )
	# plt.tight_layout(pad=0.1)
	# plt.savefig('./figs/cnt_tempscatter.pdf')
	# plt.show()
	# plt.close()


	hexplot = sns.jointplot(
							x='temp', y='cnt', data=data,
							kind='hex', height=4
						).set_axis_labels(
						'temperature/$^\circ$C',
						'count of rented bikes/hour'
						)

	plt.subplots_adjust(left=0.1, right=0.85, top=0.85, bottom=0.15)
	plt.colorbar(cax=hexplot.fig.add_axes([.9, .25, .03, .4]), label='density')

	# keep tightbox
	hexplot.savefig('./figs/cnt_tempscatter.pdf')
	plt.close()



if False:

	hexplot = sns.jointplot(
							x='hum', y='cnt', data=data,
							kind='hex', height=4
						).set_axis_labels(
						'humidity (\%)',
						'count of rented bikes/hour'
						)
	
	plt.subplots_adjust(left=0.1, right=0.85, top=0.85, bottom=0.15)
	plt.colorbar(cax=hexplot.fig.add_axes([.9, .25, .03, .4]), label='density')
	
	hexplot.savefig('./figs/cnt_humscatter.pdf')
	plt.close()



if False:

	hexplot = sns.jointplot(
							x='windspeed', y='cnt', data=data,
							kind='hex', height=4
						).set_axis_labels(
						'wind speed',
						'count of rented bikes/hour'
						)

	plt.subplots_adjust(left=0.1, right=0.85, top=0.85, bottom=0.15)
	plt.colorbar(cax=hexplot.fig.add_axes([.9, .25, .03, .4]), label='density')

	hexplot.savefig('./figs/cnt_windspeedscatter.pdf')
	plt.close()


# # # # # # # # # # # # #  linear predictive model  # # # # # # # # # # # # # # 


if False:

	features = [
		'yr', 'season', 'mnth', 'hr', 'workingday',
		'weathersit', 'temp', 'hum', 'windspeed'
	]
	target = 'cnt'

	train, test = train_test_split(data, train_size=0.8, shuffle=True)

	# reg = linear_model.LinearRegression()
	reg = ensemble.RandomForestRegressor(n_estimators=20, max_features=None)
	reg.fit(train[features], train[target])

	pred = reg.predict(test[features])
	if (pred<0).any():
		warn('negative values in model prediction')

	print('r2_score = %.3f'%(r2_score(test[target], pred)))
	print('mean absolute error = %.3f'%(mean_absolute_error(test[target], pred)))


	plt.figure(figsize=(4,0.9*4))
	sns.scatterplot(
		test[target], pred, hue=test['yr'], hue_order=[0, 1],
		s=10, alpha=0.8, palette=['purple', 'seagreen'],
	)
	sns.regplot(test[target], pred, color='k', scatter=False)

	lgd = plt.gca().get_legend_handles_labels()
	lgd_new = (lgd[0], ['year', '2012', '2013'])
	plt.legend(*lgd_new)

	plt.xlabel('test data (cnt)')
	plt.ylabel('prediction')

	# show(pad=0.1, save='./figs/regplot.pdf')


embed()