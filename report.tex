\documentclass[11pt,a4paper]{report}
\usepackage[latin1]{inputenc}
\usepackage[margin=2cm]{geometry}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{lipsum}
\usepackage[colorlinks=true,linkcolor=red]{hyperref}
\usepackage[capitalize]{cleveref}
\usepackage{newpxtext,newpxmath}

\graphicspath{{figs/}}
\begin{document}

\begin{center}
\textbf{Bike Sharing System}
\end{center}

\subsubsection*{Part I.1.\qquad Explorative data analysis}
To begin with, let us visualize our data as time series. 
In~\cref{fig:ts_daily}, we plot the rolling average of the daily count of 
rented bikes in the system, i.e. the cumulated count per hour over a period of 
a day.
The averaging aims at smoothing out short-term fluctuations on a scale of a few 
days, and thus, highlighting long-term trends.
Clearly, our data exhibits a seasonal modulation with the peak rental 
activities observed during summer and fall. 
The number users then slowly decreases over winter and increases again in 
the course of spring.
This modulation appears to be nicely tracked by the seasonal evolution of the 
atmospheric temperatures.
On the other hand, we also remark a net increase in the total number of users 
after the first year.
Indeed, this is because the number of registered users has sensibly increased, 
compared to the casual users, suggesting a gain in popularity of the company.

Next, let us focus now on the count of rented bikes per hour \emph{(cnt)}.
The distribution of \emph{cnt} over daily hours is presented 
in~\cref{fig:cnt_hourbin}, where we also contrasted the utilization times 
during working and non-working days. During working days, the number 
of rental is higher in the morning (before 8pm) and late afternoon (after 5pm) 
--- while during non-working days, the optimal period of rental is between 11am 
and 4pm. Besides, we note that the overall number of daily activities also 
slightly varies over course of the week, with an average of 4300 bikes rented 
per day during the weekends and 4600 during the weekdays.

The number of hourly utilization of the system is also influenced by the 
weather status.
This is illustrated in~\cref{fig:cnt_weathersitbin}, where the meteorological 
conditions are ranked from 1 (very good) to 4 (very bad).
Intuitively, there are more users when the weather is good. The average number 
of users is estimated to approximately 200/hour and 
it decreases by roughly 50\% when the weather is bad (condition 3).
We note that these observations were also found consistent across all seasons 
of the year.
On the other hand, it turns out that the weather conditions, in this location, 
are generally good for $\gtrsim 90$\% of the time (condition 1+2), and thus, 
making it conductive to biking.

For definiteness, we also study the correlation of the number of rented bikes 
with the atmospheric conditions such as temperature, humidity and wind 
speed 
(see~\cref{fig:cnt_tempscatter,fig:cnt_humscatter,fig:cnt_windspeedscatter}, 
respectively).
Again, the environmental conditions here represent a certain decisiveness 
factors. In particular, we find that warmer temperatures but also less humidity 
encourage more users. The influence of the wind speed is however less obvious, 
discarding a simple linear relationship.

\subsubsection*{Part I.2.\qquad Predictive model}

Our target is the number of hourly utilization of bikes \emph{(cnt)} 
in the system.
In this model, we will not treat \emph{cnt} as a stochastic time series.
Rather, we argue that the environmental and seasonal settings are sufficiently 
enough to capture the hourly evolution of \emph{cnt} on a daily basis.
That is, we shall ignore possible autocorrelation effects and we assume that 
the hourly count data are independent variables, allowing us to shuffle the 
dataset.
In addition, we include the \emph{year} variable as a merit factor to gauge the 
overall performance of the rental system.
Therefore, the chosen prediction features are
\begin{itemize}
\item[-] \emph{year:} account for the gain in popularity of the company,
\item[-] \emph{season, month:} seasonal effects and monthly trends,
\item[-] \emph{hour, working day:} utilization times and 
weekday/weekends/holidays effects,
\item[-] \emph{weather situations, temperatures, humidity, and wind speed:} 
environmental factors.
\end{itemize}
We choose a Random Forest (RF) regression algorithm to make the prediction. RF 
is popular for its generally good accuracy and fast training time.
80\% of the dataset is used for training the model and the remaining 20\% for 
testing.
Our result is presented in~\cref{fig:cnt_regplot}, where we obtain a good 
agreement between the test and prediction data, with a coefficient of 
determination of $\boldsymbol{r^2 \approx 0.92}$ and a mean absolute error of
$\boldsymbol{MAE \approx 28}$. Notice that the latter is roughly 10\% 
of the observed average \emph{cnt} during peak activity times, as seen 
in~see~\cref{fig:cnt_hourbin}.

Another advantage of the RF algorithm is its fast prediction time. For 
production ready code, it is easy to write a high-level class that takes some 
input features, and then, compute for instance a daily prediction.
For our model, we find that the relative importances of the prediction 
variables are as follow: \emph{hour $({\sim}0.60)$ -- temperature 
$({\sim}0.14)$ -- year $({\sim}0.08)$ -- working day $({\sim}0.06)$ 
-- humidity -- season -- weather situation -- month -- windspeed}.
Since only the environmental factors would require manual inputs, it is also 
conceivable to set up our API to also retrieve the local meteorological 
forecasts and automatically update a daily prediction curve.


\subsubsection*{Part II.\qquad Scaling}

Let us now suppose that we deal with large datasets. Here, the issue is 
\emph{at least} 3 dimensional:
storage format (size/flexibility/fast IO),
processing (disk-based/memory-based), and
computational times (scalable algorithm).
 
First, the inconvenience of using data structure such as 
pandas.DataFrame or numpy.ndarray, despite their great flexibility, is that the 
requested dataset needs to be entirely loaded in memory.
Especially, with pandas the required memory size would be typically at least 
twice the raw file-size, due additional overhead at reading time.
For not too large datasets (${\sim}10$\,GB, CSV), this issue can very often be 
overcome by a clever preprocessing, and then, storing the output into a 
convenient data format.
Indeed in most cases, we would not need to use the entire dataset all at the 
same time.

It is good practice to only load the relevant columnar data, assign 
proper data types (e.g. 1-byte integer for binary data), pre-compute the 
quantities 
of interest (e.g. average per bin, which would also reduce the dimensionality), 
and then dump the newly formed data-structure into a binary format (e.g. 
npy/npz, pickle) for future processing.
Binary formats are faster to load and the preprocessing enables an optimal 
usage of the available memory.
For larger datasets, I would opt for HDF5 as storage format as it would allow 
us to seamlessly stream several terabytes of data without blowing up the 
computer memory, since the reading is essentially disk-based.

Coming now to building a predictive model, much of the computational issues can 
be addressed by using parallelizable machine learning algorithms (that is the 
case for our RF model).
The real advantage of such algorithms is the ability to split the job processes 
onto multiple processors or machines, thus boosting significantly the 
computational power.
For very large datasets, these algorithms can also leverage large scale 
distributed systems such as Hadoop or Spark.
One drawback, of course, is in the costs of using computing clusters in order 
to get a decent level of parallelization.

From my three years experiences of dealing regularly with a few tens of 
gigabytes of data from experiments or simulations,  I chose to always finely 
optimize the pre-processing and dump the transformed data into npz binary 
arrays, pickle or HDF5, as appropriate.
These structures are fast to process and easily fit in memory.
With my growing interest in big data on distributed systems, I am also 
currently experimenting PySpark and its MLlib library.


\clearpage
\begin{figure}[t]\centering
\includegraphics[width=0.9\textwidth]{ts_daily}
\caption{
Rolling average (over 3 days) of the daily count of rental 
bikes \emph{cnt}, spanning a period of two years and showing a clear seasonal 
modulation. While the average number of \emph{casual} users is typically the 
same for each period of the year, the number of rental from 
\emph{registered} users shows a 
net increase in the second year, resulting in a overall increase in \emph{cnt}. 
The weather temperature is also shown for comparison and exhibits a nice 
correlation with \emph{cnt}.
The various colored regions indicates the seasons  (spring - summer - 
fall - winter, cycling from left to right).
\label{fig:ts_daily}
}
\end{figure}

\clearpage
\begin{figure}[t]\centering
\includegraphics[width=0.9\textwidth]{cnt_hourbin}
\caption{
Distribution of the hourly count of rented bikes, binned per daily hour and 
split based on whether it's a working day (i.e week days) or not (i.e 
weekend or public holiday).
During working days, the utilization times peak at early time in the 
morning and at the end of the day, which naturally match the times when 
people go to or leave from work, respectively.
In contrast, for non-working days, the number of users is higher in the middle 
of day.
The gray-shaded region simply indicates the period from 6pm to 6am, where the 
overall number of rental decreases progressively.
\label{fig:cnt_hourbin}
}
\end{figure}

\clearpage
\begin{figure}[t]\centering
\includegraphics[width=0.9\textwidth]{cnt_weathersitbin}
\caption{
Distribution of the hourly count of rented bikes, binned with 
respect to the local weather conditions ranked from 1 (very good) to 4 (very 
bad).
The sizes of the markers are indicative of the relative densities of the data 
points and the annotated numbers represent the overall fractions of data per 
bin.
Thus, the 3 points in the last bin suggest outliers, which actually correspond 
to particularly bad weather conditions that had occurred in 2011-01-26, 
2012-01-09 and 2012-01-21.
The horizontal dashed-lines correspond to the average hourly activities for 
each bin.
In general, there tends to be a higher number of users when the weather 
situation is favorable, as we would expect.
\label{fig:cnt_weathersitbin}
}
\end{figure}

\clearpage
\begin{figure}[t]\centering
\includegraphics[width=0.9\textwidth]{cnt_tempscatter}
\caption{
Density scatter-plot of the hourly count of rented bikes and the atmospheric 
temperature. The unbinned Pearson correlation coefficient is 
estimated to 0.40, which indicates a non-negligible (linear) correlation 
between the two data, as already suggested in~\cref{fig:ts_daily}.
Warmer temperatures suggest good weather conditions, making thus people likely 
inclined to use bikes to move around, which is also in agreement 
with~\cref{fig:cnt_weathersitbin}.
\label{fig:cnt_tempscatter}
}
\end{figure}

\clearpage
\begin{figure}[t]\centering
\includegraphics[width=0.9\textwidth]{cnt_humscatter}
\caption{
Density scatter-plot of the hourly count of rental bikes and the atmospheric 
humidity. Here the two data are anti-correlated, with an unbinned Pearson 
correlation coefficient of $-0.32$.
Higher humidity is likely not conductive to the utilization of bikes.
\label{fig:cnt_humscatter}
}
\end{figure}


\clearpage
\begin{figure}[t]\centering
\includegraphics[width=0.9\textwidth]{cnt_windspeedscatter}
\caption{
Density scatter-plot of the hourly count of rented bikes and the wind speed.
Although it appears that windy periods are not suitable for bike users, 
and thus resulting in less rental recorded, the dependence between the two 
quantities is non-trivial, suggesting a non-linear relationship.
\label{fig:cnt_windspeedscatter}
}
\end{figure}


\clearpage
\begin{figure}[t]\centering
\includegraphics[width=0.8\textwidth]{regplot}
\caption{
Prediction of the number rented bikes by using a random forest regression 
algorithm. Only for plotting purpose, in order to highlight the good 
performance of the prediction, the test data has been explicitly split between 
the two years, as indicated by the different marker colors. 
\label{fig:cnt_regplot}
}
\end{figure}

\end{document}