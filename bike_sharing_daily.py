""" plot rolling average of count of rental bikes per day """
#!usr/bin/env python3

import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt 

from IPython import embed
from matplotlib.patches import Rectangle
from utils import *

pd.set_option('display.max_rows', 20)
plt.rcParams.update(rcMpl)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 


data = read_data('./data/day.csv')
data = data.set_index('dteday')

count = data[['casual', 'registered', 'cnt']]
count_rav = count.rolling(3).mean()

temps = data[['temp', 'atemp']]
temps_rav = temps.rolling(3).mean()

season = get_season_period(data)


fig, ax = plt.subplots(figsize=(5,0.6*5))

count_rav.plot(y='cnt', lw=1, zorder=3, legend=False, ax=ax)
count_rav.plot(y='casual', lw=0.5, legend=False, ax=ax)
count_rav.plot(y='registered', lw=0.5, legend=False, ax=ax)


y0, y1 = ax.get_ylim()
season_color = ['palegreen', 'skyblue', 'bisque', 'lightgrey']

for i in range(len(season.index)-1):
	x0, x1 = season.index[i], season.index[i+1]
	ax.add_patch(Rectangle(
		(x0,y0), x1-x0, y1-y0, lw=0,
		color=season_color[season.loc[x0]-1], alpha=0.6,
	))

ax.set_xlabel('')
ax.set_ylabel('count of rented bikes/day')
lgd = ax.legend(
	bbox_to_anchor=(0, 1.01), loc='lower left',
	ncol=3, columnspacing=2, frameon=False
)
for line in lgd.get_lines():
	line.set_linewidth(2)


ax2 = ax.twinx()
temps_rav.plot(y='temp', lw=1, color='k', legend=False, ax=ax2)
ax2.set_ylabel('temperature/$^\circ$C')
lgd2 = ax2.legend(
	bbox_to_anchor=(0.8, 1.01), loc='lower left',
	frameon=False
)
for line in lgd2.get_lines():
	line.set_linewidth(2)


show(pad=0.1, save='./figs/ts_daily.pdf')


embed()