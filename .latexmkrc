@default_files = ('report.tex');
$pdf_mode = 1;
$pdflatex ='pdflatex -synctex=1';
$pdf_previewer = 'start evince';
$clean_ext = "tex~ bbl blg spl soc mtc* maf tdo nav snm fff ttt bcf run.xml synctex.gz";
